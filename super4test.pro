#-------------------------------------------------
#
# Project created by QtCreator 2013-10-12T14:12:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = super4test
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    super4.cpp

HEADERS  += mainwindow.h \
    irealyset.h \
    super4.h

FORMS    += mainwindow.ui


win32: LIBS += -L$$PWD/../../../../../opencv/build/x86/vc10/lib/ -lopencv_core245
win32: LIBS += -L$$PWD/../../../../../opencv/build/x86/vc10/lib/ -lopencv_highgui245
win32: LIBS += -L$$PWD/../../../../../opencv/build/x86/vc10/lib/ -lopencv_imgproc245
win32: LIBS += -L$$PWD/../../../../../opencv/build/x86/vc10/lib/ -lopencv_calib3d245
win32: LIBS += -L$$PWD/../../../../../opencv/build/x86/vc10/lib/ -lopencv_features2d245

INCLUDEPATH += $$PWD/../../../../../opencv/build/include
DEPENDPATH += $$PWD/../../../../../opencv/build/include

QMAKE_LFLAGS_DEBUG += /NODEFAULTLIB:atlthunk.lib
QMAKE_LFLAGS_RELEASE += /NODEFAULTLIB:atlthunk.lib
QMAKE_LFLAGS_DEBUG += /NODEFAULTLIB:LIBCMT.lib
QMAKE_LFLAGS_RELEASE += /NODEFAULTLIB:LIBCMT.lib


win32: LIBS += -L$$PWD/../../../../../opencv/3rdparty/ -lvideoInput

INCLUDEPATH += $$PWD/../../../../../opencv/3rdparty/include
DEPENDPATH += $$PWD/../../../../../opencv/3rdparty/include

