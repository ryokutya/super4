#ifndef SUPER4_H
#define SUPER4_H

#include <QLibrary>
#include <QDebug>

#include <iostream>

//USB relay contorol
//int32_t SetRelays(char* relayBoard, uint8_t mask, uint8_t set)
typedef int (*SetRelays)(char*, int, int);
void USBrelay(std::string, int, int);

//void USBrelay(std::string device, int relay, int set){
//    /*
//     Set or un-set the relays.
//    The first 4 bits of the 'mask' determines which relays will be affected:
//        1 = relay will be affected
//        0 = leave alone (don't change)

//    'set' determines whether the relay will be switched on or switched off.
//    1 = relay on
//    0 = relay off.

//    example: To control relay 1 and 3 and leave relay 2 and 4 unchanged, mask = 5 (0101)

//    Returns:
//        The state of the relays after the action.
//        0 to 15:
//        first 4 bits represent the relay states.
//        1 = relay is on.
//        0 = relay is off.

//        A negative number indicates an error:

//        -1 invalid relay board string (board not found)
//        -2 DLL function not loaded (FTD2XX.DLL not found)
//        -3 Invalid bit mask (must be 0 to 15)
//        -4 Undefined error
//     */
//    QLibrary myusb4dll("super4");
//    if(myusb4dll.load()){
//        std::cout << "usb4 load"<<std::endl;
//        SetRelays func = (SetRelays)myusb4dll.resolve("SetRelays");

//        std::cout <<func<<std::endl;
//        if(func){

//            //char std convert
//            int len = device.length();
//            char* fname = new char[len+1];
//            memcpy(fname, device.c_str(), len+1);
//            //char std convert

//             int a = func(fname, relay, set);
//            std::cout <<"func"<<std::endl;
//             std::cout <<a<<std::endl;
//        }else{
//            std::cout <<"no func"<<std::endl;
//        }

//    }else{
//        std::cout << "usb4 unload"<<std::endl;
//    }
//    myusb4dll.unload();
//}
class super4
{
public:
    super4();
};

#endif // SUPER4_H
